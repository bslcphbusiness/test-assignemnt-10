const chai = require('chai');
chai.should();
const {Builder, By, Key, until} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

let location = 'http://'+process.env.location;

function getDriver() {
    return new Builder().forBrowser('chrome')
        .setChromeOptions(new chrome.Options().headless().windowSize({
                width: 1000,
                height: 1000
            })
                .addArguments("--no-sandbox")
                .addArguments("--disable-dev-shm-usage")
        )
        .build();
}

async function wrap(fn) {
    let driver = await getDriver();

    try {
        await fn(driver);
    } catch (e) {
        try {
            await driver.quit();
        } catch (e) {
        }
        throw e;
    } finally {
        try {
            await driver.quit();
        } catch (e) {
        }
    }
}

describe('Returns number', function () {
    this.timeout(50000);

    it("Rand 0 - 1", async function () {
        await wrap(async driver => {
            await driver.get(location);

            let text = await (await driver.findElement(By.css("body"))).getText();

            text.should.equal(parseFloat(text).toString());
        })
    });
})



describe("integerBetween", function () {
    this.timeout(50000);

    it("in in the right range", async function () {
        await wrap(async driver => {
            await driver.get(location+'/i/-10/-5');

            let text = await (await driver.findElement(By.css("body"))).getText();

            text.should.equal(parseFloat(text).toString());
            parseFloat(text).should.satisfy(i => [-10, -9, -8, -7, -6, -5].includes(i))
        })
    })
});
describe("integerUpTo", function () {
    this.timeout(50000);

    it("in in the right range",  async function () {
        await wrap(async driver => {
            await driver.get(location+'/i/4');

            let text = await (await driver.findElement(By.css("body"))).getText();


            text.should.equal(parseFloat(text).toString());
            parseFloat(text).should.satisfy(i => [0, 1, 2, 3, 4].includes(i))

        })

    })
});
describe("between", function () {
    this.timeout(50000);

    it("in in the right range",  async function () {
        await wrap(async driver => {
            await driver.get(location+'/15/25');

            let text = await (await driver.findElement(By.css("body"))).getText();


            text.should.equal(parseFloat(text).toString());
            parseFloat(text).should.satisfy(i => i >= 15 && i < 25)

        })

    })
});
describe("upTo", function () {
    this.timeout(50000);

    it("in in the right range",  async function () {
        await wrap(async driver => {
            await driver.get(location+'/5');

            let text = await (await driver.findElement(By.css("body"))).getText();

            text.should.equal(parseFloat(text).toString());
            parseFloat(text).should.satisfy(i => i => 0 && i < 5)

        })

    })
});
describe("rand", function () {
    this.timeout(50000);

    it("in in the right range",  async function () {
        await wrap(async driver => {
            await driver.get(location+'/');

            let text = await (await driver.findElement(By.css("body"))).getText();

            text.should.equal(parseFloat(text).toString());
            parseFloat(text).should.satisfy(i => i >= 0 && i < 1)

        })

    })
});