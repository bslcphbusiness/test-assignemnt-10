const chai = require('chai');
chai.should();
let URandom = require('../src/random');


describe("integerBetween", function () {
    it("in in the right range", function () {
        for (let iter = 1; iter <= 100; iter++) {
            URandom.integerBetween(-10, -5).should.satisfy(i => [-10, -9, -8, -7, -6, -5].includes(i))
            URandom.integerBetween(11, 14).should.satisfy(i => [11, 12, 13, 14].includes(i))
        }
    })
});
describe("integerUpTo", function () {
    it("in in the right range", function () {
        for (let iter = 1; iter <= 100; iter++) {
            URandom.integerUpTo(4).should.satisfy(i => [0, 1, 2, 3, 4].includes(i))
        }
    })
});
describe("between", function () {
    it("in in the right range", function () {
        for (let iter = 1; iter <= 100; iter++) {
            URandom.between(-10, -5).should.satisfy(i => i >= -10 && i < -5);
            URandom.between(15, 25).should.satisfy(i => i >= 15 && i < 25)
        }
    })
});
describe("upTo", function () {
    it("in in the right range", function () {
        for (let iter = 1; iter <= 100; iter++) {
            URandom.upTo(5).should.satisfy(i => i => 0 && i < 5)
        }
    })
});
describe("rand", function () {
    it("in in the right range", function () {
        for (let iter = 1; iter <= 100; iter++) {
            URandom.rand().should.satisfy(i => i >= 0 && i < 1)
        }
    })
});