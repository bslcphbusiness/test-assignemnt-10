# Test assignment 10 - CI

https://github.com/datsoftlyngby/soft2019spring-test/blob/master/Assignments/10%20CI%20CD%20Assignment.pdf

I wanted to try GitLab for a long period now, because of the CI/CD capabilities. And I am really impressed.

I used docker for the image, or for the 2 images. I have an [image](https://gitlab.com/bslcphbussiness/test-assignemnt-10/blob/master/Dockerfile) for the application with the unit tests included, and then I have another [image](https://gitlab.com/bslcphbussiness/test-assignemnt-10/blob/master/Dockerfile.integration_test) for the selenium test with chrome installed.

The ci job is run automatically by GitLab when commits are pushed, the pipeline is defined in [.gitlab-ci.yml](https://gitlab.com/bslcphbussiness/test-assignemnt-10/blob/master/.gitlab-ci.yml).

The [applicaiton](https://gitlab.com/bslcphbussiness/test-assignemnt-10/tree/master/src) is a simple web service there can produce random integers and floats with bounds defined as URL parameters. 

# The [pipeline](https://gitlab.com/bslcphbussiness/test-assignemnt-10/pipelines)

... is build up using 5 parts

>   There are 2 docker container registries in use in this pipeline, a [container registry on gitlab](https://gitlab.com/bslcphbussiness/test-assignemnt-10/container_registry) there is used to hold containers between the steps, and one on [dockerhub](https://cloud.docker.com/u/bslcphbussiness/repository/docker/bslcphbussiness/test-assignment-10-ci) for the final release

>  Each part is run totally isolated on GitLab, it isn't necessarily run on the same slave, and therefore, no assets are shared between the steps
>
>  You can have a before_script to things there have to be in place in each step
>
>  I have a docker-login to the GitLab container registry to always be able to reach my docker containers

**build**

> Builds the container for the application and pushes it to the container registry on GitLab

**unit-test**

> Runes the container and executes the NodeJS mocha unit test inside of it  

**integration-test**

> Runes the container
>
> Builds and runs the second container with selenium, connected to the fist on, and executes the integration test


**push**

> Moves the container from the GitLab container registry to dockher-hub with a "latest" tag and a git tag

**deploy**

> Connects to a server and deletes the old container and spins up a new one with the git tag. Be careful use the latest tag in productions, it can make it hard to rollback, or make you run the wrong (and maybe not that much test) version of the application.
