const express = require('express');
const random = require('./random');
const app = express();


app.get("/info", function (req, res){
    res.end("V1, See more at https://gitlab.com/bslcphbussiness/test-assignemnt-10")
})


app.get("/", function (req, res) {
    res.end(random.rand().toString());
});

app.get("/i/:upTo", function (req, res) {
    res.end(random.integerUpTo(parseFloat(req.params.upTo)).toString());
});

app.get("/i/:from/:upTo", function (req, res) {
    res.end(random.integerBetween(parseFloat(req.params.from), parseFloat(req.params.upTo)).toString());
});

app.get("/:upTo", function (req, res) {
    res.end(random.upTo(parseFloat(req.params.upTo)).toString());
});
app.get("/:from/:upTo", function (req, res) {
    res.end(random.between(parseFloat(req.params.from), parseFloat(req.params.upTo)).toString());
});

app.listen(8080, function () {
    console.log("Server is running");
});