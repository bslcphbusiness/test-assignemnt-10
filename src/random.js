function integerBetween(from, upTo) {
    return Math.round(between(from, upTo))
}

function integerUpTo(upToNum) {
    return Math.round(upTo(upToNum))
}

function difference(from, upTo) {
    return Math.abs(from - upTo);
}

function between(from, upTo) {
    return from + rand() * difference(from, upTo)
}

function upTo(upToNum) {
    return rand() * upToNum;
}

function rand() {
    return Math.random();
}

module.exports = {
    integerBetween,
    integerUpTo,
    between,
    upTo,
    rand
}