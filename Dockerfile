FROM node:11

COPY package*.json ./

RUN npm install

COPY . .

USER node

EXPOSE 8080

CMD  node src/index.js
